import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="my_module",
    version="0.0.1",
    author="Mathieu LYSAKOWSKI",
    author_email="mlysakowskimg@gmail.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mlysakowski/simple_python_module",
    packages=setuptools.find_packages(),
    install_requires=[r.strip() for r in
        open('pip_requirements.txt').read().splitlines()],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
)