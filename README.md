# My Module

This is a simple example package.
[Check source code](https://gitlab.com/mlysakowski/simple_python_module)

## How to use

Create your virtualenv (once) : `virtualenv env --no-site-packages`

- Activate your virtualenv `source env/bin/activate`
- Install pip requirements `pip install git+https://gitlab.com/mlysakowski/simple_python_module.git --upgrade`
- test 
    ```python
    import my_module
    my_module.name
    from my_module import example
    example.test_requests()
    ```
- Deactivate your virtualenv `deactivate`